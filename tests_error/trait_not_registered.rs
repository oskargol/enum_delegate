trait Foo {
    fn number(&self) -> i32;
}

struct A;

impl Foo for A {
    fn number(&self) -> i32 {
        0
    }
}

struct B;

impl Foo for B {
    fn number(&self) -> i32 {
        1
    }
}

#[enum_delegate::implement(Foo)]
enum AllFoos {
    A(A),
    B(B),
}

fn main() {}
