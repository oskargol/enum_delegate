#[enum_delegate::register]
trait Foo {
    fn selfless();
}

struct A;

impl Foo for A {
    fn selfless() {}
}

#[enum_delegate::implement(Foo)]
enum E {
    A(A),
}

fn main() {}
