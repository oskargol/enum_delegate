#[enum_delegate::register]
trait Foo {}

#[enum_delegate::implement(Foo)]
enum E {
    I,
    SolemnlySwear(),
    IAm {},
    UpToNoGood(usize, usize),
}

fn main() {}
