#[enum_delegate::register]
trait Foo: Clone {}

#[derive(Clone)]
struct A;
impl Foo for A {}

#[enum_delegate::implement(Foo)]
enum E {
    A(A),
}

fn main() {}
