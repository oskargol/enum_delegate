#[enum_delegate::register]
trait Foo<T> {
    fn foo(&self, x: T);
}

struct A;
impl Foo<i32> for A {
    fn foo(&self, x: i32) {}
}

#[enum_delegate::implement(Foo)]
enum E {
    A(A),
}

fn main() {}
