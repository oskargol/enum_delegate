#[enum_delegate::register]
trait Foo {
    type Thing;
}

struct A;

impl Foo for A {
    type Thing = String;
}

struct B;

impl Foo for B {
    type Thing = ();
}

#[enum_delegate::implement(Foo)]
enum OperatorEnum {
    A(A),
    B(B),
}

fn main() {}
