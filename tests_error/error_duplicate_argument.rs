#[enum_delegate::register]
trait Foo {
    #[enum_delegate(unify = "enum_wrap")]
    #[enum_delegate(unify = "enum_wrap")]
    type Bar;
}

struct A;

impl Foo for A {
    type Bar = ();
}

#[enum_delegate::implement(Foo)]
enum E {
    A(A),
}

fn main() {}
