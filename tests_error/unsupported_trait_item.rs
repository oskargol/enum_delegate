#[enum_delegate::register]
trait Foo {
    const C: usize;
}

struct A;

impl Foo for A {
    const C: usize = 0;
}

#[enum_delegate::implement(Foo)]
enum AllFoos {
    A(A),
}

fn main() {}
