Tests that make sure that invalid code fails to compile.

The "entry point" is a `trybuild` test in `../tests/errors.rs`