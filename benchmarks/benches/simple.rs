//! Simple benchmark, inspired by the [enum_dispatch ones](https://gitlab.com/antonok/enum_dispatch/-/tree/master/benches)

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use custom_derive::custom_derive;
use enum_derive::{enum_derive_util, EnumFromInner, EnumInnerAsTrait};
use rand::Rng;

#[enum_dispatch::enum_dispatch]
#[enum_delegate::register]
trait ReturnsValue {
    fn return_value(&self) -> usize;
}

struct Zero;

impl ReturnsValue for Zero {
    fn return_value(&self) -> usize {
        0
    }
}

struct One;

impl ReturnsValue for One {
    fn return_value(&self) -> usize {
        1
    }
}

#[enum_dispatch::enum_dispatch(ReturnsValue)]
enum EnumDispatch {
    Zero,
    One,
}

#[enum_delegate::implement(ReturnsValue)]
enum EnumDelegate {
    Zero(Zero),
    One(One),
}

custom_derive! {
    #[derive(EnumFromInner)]
    #[derive(EnumInnerAsTrait(pub inner -> &dyn ReturnsValue))]
    enum EnumAsTrait {
        Zero(Zero),
        One(One),
    }
}

const VEC_SIZE: usize = 1024;
const ITERATIONS: usize = 1000000;

fn measure_pool<T: ReturnsValue>(c: &mut Criterion, name: &'static str, pool: &[T]) {
    c.bench_function(name, |b| {
        b.iter(|| {
            for i in 0..ITERATIONS {
                black_box(pool[i % VEC_SIZE].return_value());
            }
        })
    });
}

criterion_main!(benches);
criterion_group!(
    benches,
    enum_dispatch,
    box_dyn_trait,
    enum_as_trait,
    enum_delegate
);

fn enum_dispatch(c: &mut Criterion) {
    let mut rng = rand::thread_rng();

    let pool: Vec<_> = (0..VEC_SIZE)
        .map(|_| {
            if rng.gen() {
                EnumDispatch::from(Zero)
            } else {
                EnumDispatch::from(One)
            }
        })
        .collect();
    measure_pool(c, "enum_dispatch", &pool);
}

fn enum_delegate(c: &mut Criterion) {
    let mut rng = rand::thread_rng();

    let pool: Vec<_> = (0..VEC_SIZE)
        .map(|_| {
            if rng.gen() {
                EnumDelegate::from(Zero)
            } else {
                EnumDelegate::from(One)
            }
        })
        .collect();
    measure_pool(c, "enum_delegate", &pool);
}

fn enum_as_trait(c: &mut Criterion) {
    let mut rng = rand::thread_rng();

    let pool: Vec<_> = (0..VEC_SIZE)
        .map(|_| {
            if rng.gen() {
                EnumAsTrait::from(Zero)
            } else {
                EnumAsTrait::from(One)
            }
        })
        .collect();
    c.bench_function("enum as trait", |b| {
        b.iter(|| {
            for i in 0..ITERATIONS {
                black_box(pool[i % VEC_SIZE].inner().return_value());
            }
        })
    });
}

fn box_dyn_trait(c: &mut Criterion) {
    let mut rng = rand::thread_rng();

    let pool: Vec<_> = (0..VEC_SIZE)
        .map(|_| {
            let b: Box<dyn ReturnsValue> = if rng.gen() {
                Box::new(Zero)
            } else {
                Box::new(One)
            };

            b
        })
        .collect();
    c.bench_function("Box dyn Trait", |b| {
        b.iter(|| {
            for i in 0..ITERATIONS {
                black_box(pool[i % VEC_SIZE].return_value());
            }
        })
    });
}
