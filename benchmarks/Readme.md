A simple benchmark to check that `enum_delegate` doesn't perform worse than `enum_dispatch`.

# Reproducing the `enum_dispatch` benchmark

We run the benchmark implemented in `enum_dispatch`, adding `enum_delegate` to the list of compared structures. The measurement technique, as well as large pieces of code are taken from the [`enum_dispatch` benchmarks](https://gitlab.com/antonok/enum_dispatch/-/tree/master/benches).

We compare 4 different dynamic dispatch structures:
- An enum implementing a trait `T` with `enum_dispatch`
- An enum implementing a trait `T` with `enum_delegate` (our library)
- An enum with a method that converts the inner field into a `&dyn T`
- `Box<dyn Trait>`

For testing purposes, we define a trait `ReturnsValue` with a single method returning `ReturnsValue`. We then define 2 unit structs, `Zero` and `One`, for which we implement this trait. The implemented methods return 0 and 1 respectively.

For each subject, we construct a `Vec` of 1024 items of the chosen dynamically dispatched type, and fill it with instances of `Zero` and `One` randomly.

Then, we measure how long it takes to call the method on each entry consecutively, wrapping around until we have made 1000000 calls.

# Results

Average
- `enum_dispatch`: 379 µs
- `enum_delegate`: 380 µs (this crate)
- Converting enum inner field to `&dyn trait`: 4.02 ms
- `Box<dyn Trait>`: 5.24 ms

These results are quite similar to those described in the `enum_dispatch` crate.

Importantly, our new method isn't significantly slower than `enum_dispatch`. This is not surprising, as both libraries generate similar code in this case.