# How does it work?

The implementation code generation itself should be pretty straightforward for those familiar with Rust macros, so we won't go into much detail here.

The interesting trick is how the `implement` macro works with the `register` macro.

Most generally, to generate an implementation of trait T for an enum E like we do, we need 4 things:
- (1) The path to trait T (since it might not be in scope)
- (2) The definition of trait T (to know what methods we need to delegate, etc.)
- (3) The path to enum E
- (4) The definition of enum E (to know what the variant names & types are)

The `implement` macro requires the user to pass in the path to the trait to implement (1). Since it annotates the enum, it can also see the enum definition (4) and, since the code generation happens at the call site, it can infer the path to the enum (3).

The problem is that `implement` can't see the trait definition (2). The solution is to annotate the trait with a `register` macro, and pass this information along to the `implement` macro. This would be straightforward if the macros had a good way of communicating with each other, but for technical reasons, they don't - especially when they are invoked in different crates.

The trick is to generate a macro right next to the trait. We can generate a new `macro_rules` macro that "knows" the trait definition, and generates the necessary code when given the trait path, enum path and enum declaration.

To make it available in other crates, we must `macro_export` it. This will make it available at top-level, so it needs a unique name to not clash with other traits of the same name.

```rust, ignore
    quote! {
        #[macro_export]
        macro_rules! #macro_name {
            ($trait_path: path, $enum_path: path, $enum_declaration: item) => {
                enum_delegate::implement_trait_for_enum!{
                    $trait_path,
                    #parsed_trait,
                    $enum_path,
                    $enum_declaration
                }
            };
        } // ...
    }
```

> Note: `enum_delegate::implement_trait_for_enum` is a function macro that generates the actual implementation. It is not part of the public API.

But how will the `implement` macro "find" this generated macro? To make this easy, we can use another trick - exposing the macro with the same name as the trait:

```rust, ignore
pub use #macro_name as #trait_name;
```

Now, any path to the trait is also a path to our generated macro. This doesn't cause a collision because macros and traits live in different [namespaces](https://doc.rust-lang.org/reference/names/namespaces.html).

All the `implement` macro has to do now is call this newly generated macro with the path to the trait (provided by the user), the enum path and declaration (parsed from the annotated enum).

So yeah. This library defines a macro that generates another macro used by a third macro. It was a lot of fun.
