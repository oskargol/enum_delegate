#[enum_delegate::register]
trait Foo {
    fn self_ref(&self) -> i32;
    fn self_mut(&mut self) -> i32;
    fn self_own(self) -> i32;
}

struct A;

impl Foo for A {
    fn self_ref(&self) -> i32 {
        0
    }

    fn self_mut(&mut self) -> i32 {
        0
    }

    fn self_own(self) -> i32 {
        0
    }
}

struct B;

impl Foo for B {
    fn self_ref(&self) -> i32 {
        1
    }

    fn self_mut(&mut self) -> i32 {
        1
    }

    fn self_own(self) -> i32 {
        1
    }
}

#[enum_delegate::implement(Foo)]
enum AllFoos {
    A(A),
    B(B),
}

#[test]
fn test_implementation() {
    let a = AllFoos::A(A);
    assert_eq!(a.self_ref(), 0);

    let b = AllFoos::B(B);
    assert_eq!(b.self_own(), 1);
}
