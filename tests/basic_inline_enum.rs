#[enum_delegate::implement_for{
    AllFoos,
    enum AllFoos {
        A(A),
        B(B),
    }
}]
trait Foo {
    fn number(&self) -> i32;
}

struct A;

impl Foo for A {
    fn number(&self) -> i32 {
        0
    }
}

struct B;

impl Foo for B {
    fn number(&self) -> i32 {
        1
    }
}

enum AllFoos {
    A(A),
    B(B),
}

#[test]
fn test_implementation() {
    let a = AllFoos::A(A);
    assert_eq!(a.number(), 0);

    let b = AllFoos::B(B);
    assert_eq!(b.number(), 1);
}
