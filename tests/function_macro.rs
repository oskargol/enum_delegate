struct A;

enum E {
    A(A),
}

trait Tr {}

impl Tr for A {}

enum_delegate::implement_trait_for_enum!(
    Tr,
    trait Tr {},
    E,
    enum E {
        A(A),
    }
);

#[test]
fn test() {
    assert!(impls::impls!(E: Tr))
}
