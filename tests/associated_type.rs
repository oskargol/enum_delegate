#[enum_delegate::register]
trait Operator {
    type Thing;

    fn construct(&self) -> Self::Thing;

    fn process(&self, thing: &mut Self::Thing);
}

struct A;

impl Operator for A {
    type Thing = i32;

    fn construct(&self) -> Self::Thing {
        1
    }

    fn process(&self, thing: &mut Self::Thing) {
        *thing *= 2
    }
}

struct B;

impl Operator for B {
    type Thing = i32;

    fn construct(&self) -> Self::Thing {
        2
    }

    fn process(&self, thing: &mut Self::Thing) {
        *thing *= 3
    }
}

#[enum_delegate::implement(Operator)]
enum OperatorEnum {
    A(A),
    B(B),
}

#[test]
fn test_implementation() {
    let a = OperatorEnum::A(A);
    let mut s = a.construct();
    assert_eq!(s, 1);
    a.process(&mut s);
    assert_eq!(s, 2);

    let b = OperatorEnum::B(B);
    let mut s = b.construct();
    assert_eq!(s, 2);
    b.process(&mut s);
    assert_eq!(s, 6);
}
