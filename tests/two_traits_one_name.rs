#[test]
fn it_compiles() {
    mod connor {
        #[enum_delegate::register]
        trait CanThereOnlyBeOne {}
    }

    mod kurgan {
        #[enum_delegate::register]
        trait CanThereOnlyBeOne {}
    }
}
