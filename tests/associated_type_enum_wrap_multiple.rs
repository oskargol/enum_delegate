#[enum_delegate::register]
trait Operator {
    #[enum_delegate(unify = "enum_wrap")]
    type Thing;

    #[enum_delegate(unify = "enum_wrap")]
    type AnotherThing;

    fn construct(&self) -> Self::Thing;

    fn convert_to_string(&self, thing: &Self::Thing) -> String;
}

struct A;

impl Operator for A {
    type Thing = bool;
    type AnotherThing = ();

    fn construct(&self) -> Self::Thing {
        false
    }

    fn convert_to_string(&self, thing: &Self::Thing) -> String {
        format!("here's a bool: {}", thing)
    }
}

struct B;

impl Operator for B {
    type Thing = i64;
    type AnotherThing = f64;

    fn construct(&self) -> Self::Thing {
        42
    }

    fn convert_to_string(&self, thing: &Self::Thing) -> String {
        format!("here's an integer: {}", thing)
    }
}

#[enum_delegate::implement(Operator)]
enum OperatorEnum {
    A(A),
    B(B),
}

#[test]
fn test_implementation() {
    let a = OperatorEnum::A(A);
    let s = a.construct();
    assert_eq!(a.convert_to_string(&s), "here's a bool: false");

    let b = OperatorEnum::B(B);
    let s = b.construct();
    assert_eq!(b.convert_to_string(&s), "here's an integer: 42");
}
