use std::convert::TryInto;

use impls::impls;

struct A;
struct B;

#[enum_delegate::implement_conversions]
enum E {
    A(A),
    B(B),
}

#[test]
fn assert_implemented() {
    assert!(impls!(E: From<A>));
    assert!(impls!(E: TryInto<A>));
    assert!(impls!(E: From<B>));
    assert!(impls!(E: TryInto<B>));

    // checking these is not supported by impls
    let mut e: E = A.into();
    let _tmp: &A = (&e).try_into().unwrap();
    let _tmp: &mut A = (&mut e).try_into().unwrap();

    let mut e: E = B.into();
    let _tmp: &B = (&e).try_into().unwrap();
    let _tmp: &mut B = (&mut e).try_into().unwrap();
}
