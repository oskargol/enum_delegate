#[enum_delegate::register]
trait Foo {
    fn process(&self, number: i32) -> i32;
}

struct A;

impl Foo for A {
    fn process(&self, number: i32) -> i32 {
        number * 2
    }
}

struct B;

impl Foo for B {
    fn process(&self, number: i32) -> i32 {
        number * 3
    }
}

#[enum_delegate::implement(Foo)]
enum AllFoos {
    A(A),
    B(B),
}

#[test]
fn test_implementation() {
    let a = AllFoos::A(A);
    assert_eq!(a.process(5), 10);

    let b = AllFoos::B(B);
    assert_eq!(b.process(5), 15);
}
