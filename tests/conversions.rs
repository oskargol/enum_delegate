struct A(i64);

struct B;

#[enum_delegate::register]
trait T {}
impl T for A {}
impl T for B {}

#[enum_delegate::implement(T)]
enum E {
    A(A),
    B(B),
}

#[test]
fn test_convert() {
    let e: E = A(42).into();
    let a: A = e.try_into().unwrap();

    assert!(matches!(a, A(42)));
}
