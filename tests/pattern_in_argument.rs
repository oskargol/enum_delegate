#[enum_delegate::register]
trait Foo {
    fn tricky(&self, (a, _b): (usize, usize)) -> usize {
        a
    }
}

struct A;

impl Foo for A {}

#[enum_delegate::implement(Foo)]
enum AllFoos {
    A(A),
}

#[test]
fn test_implementation() {
    let a = AllFoos::A(A);
    assert_eq!(a.tricky((0, 0)), 0);
}
