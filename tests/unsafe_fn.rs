#[enum_delegate::register]
trait Foo {
    unsafe fn foo(&self);
}

struct A;

impl Foo for A {
    unsafe fn foo(&self) {}
}

#[enum_delegate::implement(Foo)]
enum AllFoos {
    A(A),
}

#[test]
fn test_implementation() {
    let a = AllFoos::A(A);
    unsafe { a.foo() };
}
