// The traits can have associated types too!
// By default, the specified type must be the same for all implementers that you want to use together.
#[enum_delegate::register]
trait Operation {
    type Operand;

    fn compute(&self, x: Self::Operand) -> Self::Operand;
}

struct Add(i64);
impl Operation for Add {
    type Operand = i64;

    fn compute(&self, x: Self::Operand) -> Self::Operand {
        x + self.0
    }
}

struct Multiply(i64);
impl Operation for Multiply {
    type Operand = i64;

    fn compute(&self, x: Self::Operand) -> Self::Operand {
        x * self.0
    }
}

// Watch out - if you try to mix different associated types together, it won't compile!
// For example, try changing one of the `Operand`s to i32
#[enum_delegate::implement(Operation)]
enum AnyOperation {
    Add(Add),
    Multiply(Multiply),
}

#[test]
fn test_operations() {
    let operations: Vec<AnyOperation> = vec![Multiply(3).into(), Add(1).into()];

    let mut value = 5;
    for operation in operations {
        value = operation.compute(value);
    }

    assert_eq!(value, 16);
}
