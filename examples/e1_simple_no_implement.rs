// If for some reason you want to implement your trait for an enum in another library, you can.
// Since you aren't able to annotate the enum, you can use the `implement_for` macro instead.
#[enum_delegate::implement_for(People,
    enum People {
        Arthur(Arthur),
        Pablo(Pablo),
    }
)]
trait SayHello {
    fn say_hello(&self, name: &str) -> String;
}

struct Arthur;
impl SayHello for Arthur {
    fn say_hello(&self, name: &str) -> String {
        format!("Hello, {name}!")
    }
}

struct Pablo;
impl SayHello for Pablo {
    fn say_hello(&self, name: &str) -> String {
        format!("Hola, {name}!")
    }
}

// Pretend this is in another crate and you can't touch it
enum People {
    Arthur(Arthur),
    Pablo(Pablo),
}

#[test]
fn test_people() {
    // Unfortunately, we can't generate conversions for the enum in such cases.
    // You'll have to find another way to construct the enum instances.
    let p: Vec<People> = vec![People::Arthur(Arthur), People::Pablo(Pablo)];

    assert_eq!(p[0].say_hello("enum_delegate"), "Hello, enum_delegate!");
    assert_eq!(p[1].say_hello("enum_delegate"), "Hola, enum_delegate!");
}
