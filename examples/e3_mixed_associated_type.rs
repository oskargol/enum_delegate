// What if you want to mix different associated types together?
// You can do that by annotating the type with `#[enum_delegate(unify = "enum_wrap")]`
// Then, `enum_delegate` will generate a new enum to wrap all the possible types together

#[enum_delegate::register]
trait FavoriteThing {
    #[enum_delegate(unify = "enum_wrap")]
    type Thing;

    fn use_thing(&self, thing: Self::Thing);
}

#[derive(Debug)]
struct CupOfTea;
struct Arthur;
impl FavoriteThing for Arthur {
    type Thing = CupOfTea;

    fn use_thing(&self, cup_of_tea: Self::Thing) {
        println!("I am Arthur and I am sipping my {cup_of_tea:?}")
    }
}

#[derive(Debug)]
struct TheForce;
struct Luke;
impl FavoriteThing for Luke {
    type Thing = TheForce;

    fn use_thing(&self, the_force: Self::Thing) {
        println!("I am Luke and I am using {the_force:?}")
    }
}

#[enum_delegate::implement(FavoriteThing)]
enum AnyCharacter {
    Arthur(Arthur),
    Luke(Luke),
}

#[test]
fn test_characters() {
    let people: Vec<AnyCharacter> = vec![Luke.into(), Arthur.into()];

    // Use the force, Luke!
    people[0].use_thing(TheForce.into());

    // Drink your tea, Arthur!
    people[1].use_thing(CupOfTea.into());
}

#[test]
#[should_panic]
fn oh_no() {
    let people: Vec<AnyCharacter> = vec![Luke.into(), Arthur.into()];

    // The force isn't exactly Arthur's cup of tea.
    // He's gonna panic if we make him drink it.
    // Drink responsibly. Don't panic.
    people[1].use_thing(TheForce.into());
}
