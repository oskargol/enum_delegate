// If the SayHello trait was in a different crate, you wouldn't be able to register it.
// But that's fine!
trait SayHello {
    fn say_hello(&self, name: &str) -> String;
}

struct Arthur;
impl SayHello for Arthur {
    fn say_hello(&self, name: &str) -> String {
        format!("Hello, {name}!")
    }
}

struct Pablo;
impl SayHello for Pablo {
    fn say_hello(&self, name: &str) -> String {
        format!("Hola, {name}!")
    }
}

// If the trait is not registered, you need to copy-paste it as the second argument:
#[enum_delegate::implement(SayHello,
    trait SayHello {
        fn say_hello(&self, name: &str) -> String;
    }
)]
enum People {
    Arthur(Arthur),
    Pablo(Pablo),
}

#[test]
fn test_people() {
    let p: Vec<People> = vec![Arthur.into(), Pablo.into()];

    assert_eq!(p[0].say_hello("enum_delegate"), "Hello, enum_delegate!");
    assert_eq!(p[1].say_hello("enum_delegate"), "Hola, enum_delegate!");
}
