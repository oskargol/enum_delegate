// Let's define a trait. For it to work with this library, we need to register it:
#[enum_delegate::register]
trait SayHello {
    fn say_hello(&self, name: &str) -> String;
}

// Now, we need some structs that implement the trait...

struct Arthur;
impl SayHello for Arthur {
    // Arthur speaks English (with a cool british accent)
    fn say_hello(&self, name: &str) -> String {
        format!("Hello, {name}!")
    }
}

struct Pablo;
impl SayHello for Pablo {
    // Pablo speaks Spanish
    fn say_hello(&self, name: &str) -> String {
        format!("Hola, {name}!")
    }
}

// Finally, let's create an enum that can represent any kind of `SayHello`.
// It will work just like a `dyn SayHello`, but faster.
// Let the `implement(SayHello)` attribute macro work its magic.
#[enum_delegate::implement(SayHello)]
enum People {
    Arthur(Arthur),
    Pablo(Pablo),
}

#[test]
fn test_people() {
    // Traits like From<Arthur> and TryInto<Arthur> are automatically implemented
    let p: Vec<People> = vec![Arthur.into(), Pablo.into()];

    assert_eq!(p[0].say_hello("enum_delegate"), "Hello, enum_delegate!");
    assert_eq!(p[1].say_hello("enum_delegate"), "Hola, enum_delegate!");
}
