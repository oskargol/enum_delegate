#![doc = include_str!("../Readme.md")]

use proc_macro::TokenStream;

/// Annotate an enum with this to implement a specified trait
///
/// When `implement(SomeTrait)` is used on `SomeEnum`, it generates the following implementations:
/// - `SomeTrait for SomeEnum`
/// - For each `SomeEnum` variant `VariantName(VariantType)`:
///     - `From<VariantType> for SomeEnum`
///     - `TryInto<VariantType> for SomeEnum`
///     - `TryInto<&VariantType> for &SomeEnum`
///     - `TryInto<&mut VariantType> for &mut SomeEnum`
///
/// All enum fields must be of the form `VariantName(VariantType)`
///
/// # Registered Traits
///
/// If you can annotate the trait you wish to implement with [macro@register], the API is really simple. Simply pass the trait as the only argument to this attribute.
///
/// Syntax: `implement(trait name)`
///
/// Example:
/// ```
/// #[enum_delegate::register]
/// trait Foo {}
///
/// struct Bar;
/// impl Foo for Bar {}
///
/// #[enum_delegate::implement(Foo)]
/// enum FooEnum {
///     Bar(Bar),
/// }
/// ```
///
/// # Unregistered Traits
///
/// In some cases, you may not be able to [macro@register] a trait (for example, if it lives in a 3rd-party crate). You can still use this attribute, but you'll have to pass in the trait definition as the second argument.
///
/// Syntax: `implement(trait name, trait definition)`
///
/// Example:
/// ```
/// trait Foo {}
///
/// struct Bar;
/// impl Foo for Bar {}
///
/// #[enum_delegate::implement{
///     Foo,
///     trait Foo {}
/// }]
/// enum FooEnum {
///     Bar(Bar),
/// }
/// ```
///
/// See the [crate-level documentation](crate) for more information.
#[proc_macro_attribute]
pub fn implement(attributes: TokenStream, item: TokenStream) -> TokenStream {
    enum_delegate_lib::implement(attributes.into(), item.into()).into()
}

/// Annotate a trait with this so that it can be more easily used with [macro@implement]
///
/// Example:
///
/// ```
/// #[enum_delegate::register]
/// trait Foo {}
///
/// struct Bar;
/// impl Foo for Bar {}
///
/// #[enum_delegate::implement(Foo)]
/// enum FooEnum {
///     Bar(Bar),
/// }
/// ```
///
/// Note: this macro generates a macro with the same name as the trait it is used on. This may cause a conflict if used together with another macro library that also generates such a macro. A workaround is to not register the trait (see [macro@implement] documentation for how you can still implement it)
///
/// See the [crate-level documentation](crate) for more information.
#[proc_macro_attribute]
pub fn register(attributes: TokenStream, item: TokenStream) -> TokenStream {
    enum_delegate_lib::register(attributes.into(), item.into()).into()
}

/// Annotate a trait with this to implement it for a specified enum
///
/// When `implement_for(SomeEnum)` is used on `SomeTrait`, it implements `SomeTrait for SomeEnum`. Unlike [macro@implement], it does not generate conversion implementations for `SomeEnum`.
///
/// All enum fields must be of the form `VariantName(VariantType)`
///
/// Syntax: `implement_for(enum name, enum definition)`
///
/// Example:
///
/// ```
/// #[enum_delegate::implement_for{
///     FooEnum,
///     enum FooEnum {
///         Bar(Bar),
///     }
/// }]
/// trait Foo {}
///
/// struct Bar;
/// impl Foo for Bar {}
///
/// enum FooEnum {
///     Bar(Bar),
/// }
/// ```
///
/// See the [crate-level documentation](crate) for more information.
#[proc_macro_attribute]
pub fn implement_for(attributes: TokenStream, item: TokenStream) -> TokenStream {
    enum_delegate_lib::implement_for(attributes.into(), item.into()).into()
}

/// Annotate an enum with this to implement [`From`] and [`TryInto`] conversions for it.
///
/// When `implement_conversions` is used on `SomeEnum`, it generates the following implementations:
/// - For each `SomeEnum` variant `VariantName(VariantType)`:
///     - `From<VariantType> for SomeEnum`
///     - `TryInto<VariantType> for SomeEnum`
///     - `TryInto<&VariantType> for &SomeEnum`
///     - `TryInto<&mut VariantType> for &mut SomeEnum`
///
/// All enum fields must be of the form `VariantName(VariantType)`
#[proc_macro_attribute]
pub fn implement_conversions(attributes: TokenStream, item: TokenStream) -> TokenStream {
    enum_delegate_lib::implement_conversions(attributes.into(), item.into()).into()
}

/// Not part of the public API; used by `[register]`-generated code
#[doc(hidden)]
#[proc_macro]
pub fn implement_trait_for_enum(attributes: TokenStream) -> TokenStream {
    enum_delegate_lib::implement_trait_for_enum(attributes.into()).into()
}
