#[enum_delegate::register]
pub trait RegisteredTrait {
    fn foo(&self);
}

pub mod nested {
    #[enum_delegate::register]
    pub trait DeeperRegisteredTrait {
        fn foo(&self);
    }
}

pub trait PlainTrait {
    fn foo(&self);
}

pub struct PlainEnumVariant;

pub enum PlainEnum {
    PlainEnumVariant(PlainEnumVariant),
}
