use dependency_crate::nested::DeeperRegisteredTrait;

struct Foo;

impl DeeperRegisteredTrait for Foo {
    fn foo(&self) {}
}

#[enum_delegate::implement(dependency_crate::nested::DeeperRegisteredTrait)]
enum RegisteredTraitEnum {
    Foo(Foo),
}

#[cfg(test)]
mod tests {
    use impls::impls;

    use super::*;

    #[test]
    fn trait_implemented() {
        assert!(impls!(RegisteredTraitEnum: DeeperRegisteredTrait))
    }
}
