use dependency_crate::PlainTrait;

struct Foo;

impl PlainTrait for Foo {
    fn foo(&self) {}
}

#[enum_delegate::implement{
    PlainTrait,
    pub trait PlainTrait {
        fn foo(&self);
    }
}]
enum RegisteredTraitEnum {
    Foo(Foo),
}

#[cfg(test)]
mod tests {
    use impls::impls;

    use super::*;

    #[test]
    fn trait_implemented() {
        assert!(impls!(RegisteredTraitEnum: PlainTrait))
    }
}
