use dependency_crate::{PlainEnum, PlainEnumVariant};

#[enum_delegate::implement_for{
    PlainEnum,
    pub enum PlainEnum {
        PlainEnumVariant(PlainEnumVariant),
    }
}]
pub trait Foo {
    fn foo(&self);
}

impl Foo for PlainEnumVariant {
    fn foo(&self) {}
}

#[cfg(test)]
mod tests {
    use impls::impls;

    use super::*;

    #[test]
    fn trait_implemented() {
        assert!(impls!(PlainEnum: Foo))
    }
}
