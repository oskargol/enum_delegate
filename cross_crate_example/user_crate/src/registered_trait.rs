use dependency_crate::RegisteredTrait;

struct Foo;

impl RegisteredTrait for Foo {
    fn foo(&self) {}
}

#[enum_delegate::implement(RegisteredTrait)]
enum RegisteredTraitEnum {
    Foo(Foo),
}

#[cfg(test)]
mod tests {
    use impls::impls;

    use super::*;

    #[test]
    fn trait_implemented() {
        assert!(impls!(RegisteredTraitEnum: RegisteredTrait))
    }
}
