This directory contains test crates to ensure that the library can be used across crates. Cross-crate usage is a special case because:

- Foreign traits (e.g. `From`) cannot be implemented on foreign types (e.g. an enum in another crate)
- Some of our macros generate macros used by other macros, which is about as tricky as it sounds, and trickier still if it has to work across crates.
