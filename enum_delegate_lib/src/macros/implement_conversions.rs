use proc_macro2::TokenStream;
use quote::{quote, quote_spanned};
use syn::spanned::Spanned;
use syn::{parse2, parse_quote, ItemEnum};

use crate::generate::enum_conversion::generate_enum_conversions;
use crate::input::single_field_enum::parse_enum_variants;

/// The `implement_conversions` macro implementation.
///
/// See the `enum_delegate` crate for more information.
// we're mirroring how macro signatures look
#[allow(clippy::needless_pass_by_value)]
#[must_use]
pub fn implement_conversions(attribute_args: TokenStream, item: TokenStream) -> TokenStream {
    if !attribute_args.is_empty() {
        let span = attribute_args.span();
        return quote_spanned!(span => compile_error!("this macro does not accept arguments"););
    }

    let item_enum: ItemEnum = match parse2(item.clone()) {
        Ok(parsed) => parsed,
        Err(error) => return error.to_compile_error(),
    };

    let parsed_enum = match parse_enum_variants(&item_enum) {
        Ok(parsed) => parsed,
        Err(error) => {
            return error.into_compiler_error();
        }
    };

    let enum_name = &item_enum.ident;
    let enum_conversions = generate_enum_conversions(&parse_quote!(#enum_name), &parsed_enum);

    quote! {
        #item
        #enum_conversions
    }
}
