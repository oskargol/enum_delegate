use proc_macro2::TokenStream;
use syn::parse::{Parse, ParseStream};
use syn::token::Comma;
use syn::{parse2, ItemEnum, ItemTrait, Path};

use crate::generate_delegation::{implement_delegation, DelegationOptions};

/// Parsed input for the `implement_trait_for_enum` macro.
///
/// Parses 4 arguments separated by a comma
struct TraitEnumArguments {
    trait_path: Path,
    trait_declaration: ItemTrait,
    enum_path: Path,
    enum_declaration: ItemEnum,
}

impl Parse for TraitEnumArguments {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        let trait_path = input.parse()?;
        let _: Comma = input.parse()?;
        let trait_declaration = input.parse()?;
        let _: Comma = input.parse()?;
        let enum_path = input.parse()?;
        let _: Comma = input.parse()?;
        let enum_declaration = input.parse()?;

        Ok(TraitEnumArguments {
            trait_path,
            trait_declaration,
            enum_path,
            enum_declaration,
        })
    }
}

/// The `implement_trait_for_enum` macro implementation.
///
/// A function-like macro implementation. Given arguments of the form (trait path, trait declaration, enum path, enum declaration), implements the specified trait for the specified enum
// we're mirroring how macro signatures look
#[allow(clippy::needless_pass_by_value)]
#[must_use]
pub fn implement_trait_for_enum(arguments: TokenStream) -> TokenStream {
    let parsed_arguments: TraitEnumArguments = match parse2(arguments) {
        Ok(parsed) => parsed,
        Err(error) => return error.to_compile_error(),
    };

    match implement_delegation(
        &parsed_arguments.trait_path,
        &parsed_arguments.trait_declaration,
        &parsed_arguments.enum_path,
        &parsed_arguments.enum_declaration,
        DelegationOptions::default(),
    ) {
        Ok(generated) => generated,
        Err(error) => error.into_compiler_error(),
    }
}
