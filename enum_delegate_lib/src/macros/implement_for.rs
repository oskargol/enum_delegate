use proc_macro2::TokenStream;
use quote::quote;
use syn::parse::{Parse, ParseStream};
use syn::token::Comma;
use syn::{parse2, parse_quote, ItemEnum, ItemTrait, Path};

use crate::generate_delegation::{implement_delegation, DelegationOptions};

/// Parsed input for the `implement_for` macro.
///
/// An enum path, followed by the enum declaration, separated by a comma
struct EnumArgumentPair {
    path: Path,
    declaration: ItemEnum,
}

impl Parse for EnumArgumentPair {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        let path = input.parse()?;
        let _: Comma = input.parse()?;
        let declaration = input.parse()?;

        Ok(EnumArgumentPair { path, declaration })
    }
}

/// The `implement_for` macro implementation.
///
/// See the `enum_delegate` crate for more information.
// we're mirroring how macro signatures look
#[allow(clippy::needless_pass_by_value)]
#[must_use]
pub fn implement_for(attribute_args: TokenStream, item: TokenStream) -> TokenStream {
    let parsed_trait: ItemTrait = match parse2(item.clone()) {
        Ok(parsed) => parsed,
        Err(error) => return error.to_compile_error(),
    };

    let trait_ident = &parsed_trait.ident;
    let trait_path = parse_quote!(#trait_ident);

    let parsed_input: EnumArgumentPair = match parse2(attribute_args) {
        Ok(parsed) => parsed,
        Err(error) => return error.to_compile_error(),
    };

    let delegation = match implement_delegation(
        &trait_path,
        &parsed_trait,
        &parsed_input.path,
        &parsed_input.declaration,
        DelegationOptions {
            generate_from_into_conversions: false,
        },
    ) {
        Ok(generated) => generated,
        Err(error) => error.into_compiler_error(),
    };

    quote! {
        #item
        #delegation
    }
}
