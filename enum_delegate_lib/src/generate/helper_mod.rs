//! Code generation for a helper mod

use proc_macro2::{Ident, TokenStream};
use quote::{format_ident, quote};
use syn::{ItemEnum, ItemTrait};

/// A unique name for the helper mod.
pub fn helper_mod_name(parsed_trait: &ItemTrait, parsed_enum: &ItemEnum) -> Ident {
    format_ident!(
        "enum_delegate_helpers_{}_for_{}",
        parsed_trait.ident,
        parsed_enum.ident,
    )
}

/// Generate the helper mod definition
pub fn generate_helper_mod(mod_name: &Ident) -> TokenStream {
    quote! {
        #[allow(non_snake_case)]
        mod #mod_name {
            /// Indicates that a pair of types (A, B) are the same
            ///
            /// Used in generated where-clauses as a workaround for https://github.com/rust-lang/rust/issues/20041
            pub trait EqualTypes: private::Sealed {}

            impl<T> EqualTypes for (T, T) {}

            mod private {
                pub trait Sealed {}

                impl<T> Sealed for (T, T) {}
            }
        }
    }
}
