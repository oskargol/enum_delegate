#![doc = include_str!("../Readme.md")]
#![warn(clippy::pedantic)]
// underscore drop seems explicit enough
#![allow(clippy::let_underscore_drop)]
// too pedantic - these are valid names
#![allow(clippy::module_name_repetitions)]
// errors are self-documenting
#![allow(clippy::missing_errors_doc)]
// improve clarity
#![deny(clippy::as_underscore, clippy::same_name_method)]
// personal preferences - lint for consistency
#![warn(clippy::if_then_some_else_none, clippy::self_named_module_files)]
// make sure these don't slip into production
#![warn(clippy::dbg_macro, clippy::todo, clippy::unimplemented)]

pub use generate_delegation::{implement_delegation, DelegationOptions};
pub use macros::*;

mod error;
mod generate;
mod generate_delegation;
mod identifiers;
mod input;
mod macros;
