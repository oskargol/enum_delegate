use proc_macro2::{Span, TokenStream};
use quote::{quote, quote_spanned};

/// Error: the input provided to the macro was malformed or not supported
#[derive(Debug)]
pub enum InvalidInput {
    /// Enums must have at least 1 variant
    NoVariants(Span),
    /// All enum variants must have a single unnamed field
    InvalidVariant(Span),

    /// All methods must take `self` as an argument, either by value or by reference, possibly mutable
    NoReceiverArgument(Span),
    /// Only functions and associated types are supported
    UnsupportedTraitItem(Span),
    /// Invalid value for an `enum_delegate` attribute
    InvalidAttributeArgument(Span),
    /// An `enum_delegate` attribute argument has been specified more than once.
    MultipleArgumentUses(Span, Span),

    /// Encountered a feature not currently supported by `enum_delegate`
    UnsupportedFeature(Span, &'static str),
}

impl InvalidInput {
    /// Convert the error into a [`TokenStream`] containing a compiler error with a message describing the issue
    pub fn into_compiler_error(self) -> TokenStream {
        match self {
            InvalidInput::NoVariants(span) => {
                quote_spanned!(span => compile_error!("enum_delegate requires enums must have at least 1 variant");)
            }
            InvalidInput::InvalidVariant(span) => {
                quote_spanned!(span => compile_error!("enum_delegate requires variants to be of the form VariantName(SomeType)");)
            }
            InvalidInput::NoReceiverArgument(span) => {
                quote_spanned!(span => compile_error!("enum_delegate requires all trait functions to take `self`, `&self` or `&mut self` as the first argument");)
            }
            InvalidInput::InvalidAttributeArgument(span) => {
                quote_spanned!(span => compile_error!("invalid enum_delegate attribute. Please see enum_delegate documentation for more information");)
            }
            InvalidInput::UnsupportedTraitItem(span) => {
                quote_spanned!(span => compile_error!("enum_delegate only supports methods and associated types in traits");)
            }
            InvalidInput::MultipleArgumentUses(span_1, span_2) => {
                let a = quote_spanned!(span_1 => compile_error!("First use of this argument. enum_delegate arguments can only be specified once per item"););
                let b = quote_spanned!(span_2 => compile_error!("Second use of this argument"););

                quote!(#a #b)
            }
            InvalidInput::UnsupportedFeature(span, feature) => {
                let message = format!("enum_delegate does not currently support {feature}");

                quote_spanned!(span => compile_error!(#message);)
            }
        }
    }
}
