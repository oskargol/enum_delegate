//! Module for parsing the input provided to the macros

pub mod associated_type;
pub mod single_field_enum;

/// Configuration for how to generate the implementation of a particular associated type
pub(crate) enum AssociatedTypeUnification {
    // The associated type has the same value in all enum branches
    Same,
    // The associated type will be an enum wrapping all the possible types
    EnumWrap,
}

impl Default for AssociatedTypeUnification {
    /// [`AssociatedTypeUnification::Same`] is the default value
    fn default() -> Self {
        AssociatedTypeUnification::Same
    }
}
