use std::collections::HashMap;

use proc_macro2::Ident;
use syn::spanned::Spanned;
use syn::{Attribute, ItemTrait, Lit, Meta, MetaNameValue, NestedMeta, TraitItem};

use crate::error::InvalidInput;
use crate::identifiers::{
    my_attribute_ident, unify_ident, TYPE_UNIFICATION_ENUM_WRAP, TYPE_UNIFICATION_SAME,
};
use crate::input::AssociatedTypeUnification;

/// Parse the configuration for each associated type of the specified enum
///
/// Returns a map from the name of the associated types to their corresponding configuration
pub(crate) fn parse_associated_type_configurations(
    parsed_trait: &ItemTrait,
) -> Result<HashMap<Ident, AssociatedTypeUnification>, InvalidInput> {
    let unify_ident = unify_ident();

    parsed_trait
        .items
        .iter()
        .filter_map(|i| match i {
            TraitItem::Type(t) => Some(t),
            _ => None,
        })
        .map(|t| {
            let relevant_attributes = parse_relevant_attribute_args(&t.attrs)?;

            if relevant_attributes.len() > 1 {
                return Err(InvalidInput::MultipleArgumentUses(
                    relevant_attributes[0].span(),
                    relevant_attributes[1].span(),
                ));
            }

            let unification = if let Some(attribute) = relevant_attributes.first() {
                let value = if let Lit::Str(string) = &attribute.lit {
                    string.value()
                } else {
                    return Err(InvalidInput::InvalidAttributeArgument(attribute.span()));
                };

                if !attribute.path.is_ident(&unify_ident) {
                    return Err(InvalidInput::InvalidAttributeArgument(attribute.span()));
                }

                if value == TYPE_UNIFICATION_SAME {
                    AssociatedTypeUnification::Same
                } else if value == TYPE_UNIFICATION_ENUM_WRAP {
                    AssociatedTypeUnification::EnumWrap
                } else {
                    return Err(InvalidInput::InvalidAttributeArgument(attribute.span()));
                }
            } else {
                AssociatedTypeUnification::default()
            };

            Ok((t.ident.clone(), unification))
        })
        .collect()
}

/// Collects individual `MetaNameValue`s from all attributes intended for our macro
fn parse_relevant_attribute_args(attrs: &[Attribute]) -> Result<Vec<MetaNameValue>, InvalidInput> {
    let my_attribute_ident = my_attribute_ident();

    attrs
        .iter()
        .filter(|a| a.path.is_ident(&my_attribute_ident))
        .flat_map(|attribute| {
            let list = if let Ok(Meta::List(list)) = attribute.parse_meta() {
                list
            } else {
                return vec![Err(InvalidInput::InvalidAttributeArgument(
                    attribute.span(),
                ))];
            };

            let args: Vec<_> = list
                .nested
                .iter()
                .map(|item| match item {
                    NestedMeta::Meta(Meta::NameValue(name_value)) => Ok(name_value.clone()),
                    other => Err(InvalidInput::InvalidAttributeArgument(other.span())),
                })
                .collect();

            args
        })
        .collect()
}
