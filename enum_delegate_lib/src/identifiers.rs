//! Conceptually constant identifiers accepted by the macros

use proc_macro2::Ident;
use quote::format_ident;

/// The identifier of attributes processed by this crate
pub(crate) fn my_attribute_ident() -> Ident {
    format_ident!("enum_delegate")
}

/// On associated types, the argument specifying the unification strategy.
///
/// Example:
///
/// ```rust, ignore
/// trait Foo {
///     #[enum_delegate(unify=enum_wrap)]
///     type Bar;
/// }
/// ```
// dreams of being a const but can't because format_ident is not const
pub(crate) fn unify_ident() -> Ident {
    format_ident!("unify")
}

/// On associated types, an argument value for `unify`.
///
/// Example:
///
/// ```rust, ignore
/// trait Foo {
///     #[enum_delegate(unify="same")]
///     type Bar;
/// }
/// ```
pub(crate) const TYPE_UNIFICATION_SAME: &str = "same";

/// On associated types, an argument value for `unify`.
///
/// Example:
///
/// ```rust, ignore
/// trait Foo {
///     #[enum_delegate(unify="enum_wrap")]
///     type Bar;
/// }
/// ```
pub(crate) const TYPE_UNIFICATION_ENUM_WRAP: &str = "enum_wrap";
